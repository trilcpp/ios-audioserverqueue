//
//  ViewController.m
//  AudioQueueServicesExample
//
//  Created by Shivansh on 6/12/17.
//
//
#import <Metal/Metal.h>
//#import <QuartzCore/CAMetalLayer.h>

#import "ViewController.h"
#import <CoreAudioKit/CoreAudioKit.h>

#import "Trigerring.hpp"                //Can only declare in .mm file,else error
#include "ShortBuffer.hpp"         //Can only declare in .mm file

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#define kDestinationURL @"http://192.168.1.23:8081/v1/iphone/audio/info/"

@interface ViewController ()
{
    AVAudioSession *session;

    id <MTLDevice> mtlDevice;
    id <MTLLibrary> library;
    id <MTLCommandQueue> mtlCommandQueue;
    id <MTLCommandBuffer> mtlCommandBuffer;
    id <MTLComputeCommandEncoder> mtlComputeCommand;
    
    MTLRenderPassDescriptor *mtlRenderPassDescriptor;
    CAMetalLayer *metalLayer;
    id <CAMetalDrawable> frameDrawable;
    CADisplayLink *displayLink;
}
@end

dispatch_queue_t concurrentQueue;
//My concurrent thread, for asynchronous task, declared outside so that can be accessed inside callback functions

// Declare C callback functions
void AudioInputCallback(void * inUserData,  // Custom audio metadata
                        AudioQueueRef inAQ,
                        AudioQueueBufferRef inBuffer,
                        const AudioTimeStamp * inStartTime,
                        UInt32 inNumberPacketDescriptions,
                        const AudioStreamPacketDescription * inPacketDescs);

void AudioOutputCallback(void * inUserData,
                         AudioQueueRef outAQ,
                         AudioQueueBufferRef outBuffer);


#pragma mark

@implementation ViewController 

@synthesize TextView;
@synthesize recordOutlet;

//Global declaration bitches
@synthesize appDelegate;
@synthesize shorts = _shorts;
@synthesize decoded = _decoded;
//------------------------------------------------------------------------------
#pragma mark - LOADER
//------------------------------------------------------------------------------
+ (ViewController *)sharedInstance {
    static dispatch_once_t onceToken;
    static ViewController *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[ViewController alloc] init];
    });
    return instance;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AudioRecorderAppDelegate *)[[UIApplication sharedApplication] delegate];
    //This ensures it plays properly on speaker output
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord
                                     withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker
                                           error:nil];
    
        refToSelf = (__bridge void*)self; //NOTE : MADE FOR SAMPLES TO ENGINE function in AudioInputCallback
    
        //..Get audio file path
        char path[256];
        [self getFilename:path maxLenth:sizeof path];
        fileURL = CFURLCreateFromFileSystemRepresentation(NULL, (UInt8*)path, strlen(path), false);
    
        // Init state variables
        playState.playing = false;
        recordState.recording = false;
    
    //Text Properties
    //self.TextView.backgroundColor = [UIColor whiteColor];   //Without setting this, text was overwritten.
    self.TextView.text=@"OTP";
    //Global SharedInstance
    _shorts = nil;
    _decoded = nil;
    //##MYTHREAD
    //concurrentQueue = dispatch_queue_create("MyQ1", DISPATCH_QUEUE_CONCURRENT);
    concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    

    //GpGPU

}


#pragma mark -- gpu
//
//-(void)initMetal{
//    // Get access to iPhone or iPad GPU
//    mtlDevice = MTLCreateSystemDefaultDevice();
//    // Queue to handle an ordered list of command buffers
//    mtlCommandQueue = [mtlDevice newCommandQueue];
//    // Buffer for storing encoded commands that are sent to GPU
//    mtlCommandBuffer = [mtlCommandQueue commandBuffer];
//    // Encoder for GPU commands
//    mtlComputeCommand=[mtlCommandBuffer computeCommandEncoder];
//    
//    //CAmetalLayer
//    metalLayer = [CAMetalLayer layer];
//    [metalLayer setDevice:mtlDevice];
//    [metalLayer setPixelFormat:MTLPixelFormatBGRA8Unorm];
//    metalLayer.framebufferOnly = YES;
//    [metalLayer setFrame:self.view.layer.frame];
//    [self.view.layer addSublayer:metalLayer];
//    [self.view setOpaque:YES];
//    [self.view setBackgroundColor:nil];
//    [self.view setContentScaleFactor:[UIScreen mainScreen].scale];
//}
//
//-(void)createComputePipeline {
//    
//    id <MTLFunction> sigmoidProgram = [library newFunctionWithName:@"sigmoid"];
//    //var pipelineErrors = NSErrorPointer()
//    NSError *pipelineErrors = nil;
//    //var computePipelineFilter = device.newComputePipelineStateWithFunction(sigmoidProgram!, error: pipelineErrors)
//    id <MTLComputePipelineState> computePipelineFilter = [mtlDevice newComputePipelineStateWithFunction:sigmoidProgram error:&pipelineErrors];
//    //computeCommandEncoder.setComputePipelineState(computePipelineFilter!)
//    [mtlComputeCommand setComputePipelineState:computePipelineFilter];
//}

//------------------------------------------------------------------------------
#pragma mark - Buttons
//------------------------------------------------------------------------------
- (IBAction)startrec:(UIButton *)sender {
    check_result_flag = 0;
    [self startRecording];
}

- (IBAction)stoprec:(UIButton *)sender {
    [self stopRecording];
    self.TextView.text= @"Stop Test";
}

- (IBAction)playrec:(UIButton *)sender {
    [self startPlayback];
}
//------------------------------------------------------------------------------
#pragma mark - Server
//------------------------------------------------------------------------------
- (void)storeToShorts: (NSString *)allBuffersString : (NSString *) pair_result
{   ViewController *globals = [ViewController sharedInstance];
    globals.shorts = allBuffersString ;
    globals.decoded = pair_result;
    [self sendServer:nil];
}

- (IBAction)sendServer:(UIButton *)sender {
    ViewController *globals = [ViewController sharedInstance];
    
    NSURL *icyURL = [NSURL URLWithString:kDestinationURL];
    NSString *vecArray = [NSString stringWithFormat: @"%@",globals.shorts];
    NSString *decArray = [NSString stringWithFormat: @"%@",globals.decoded];
    NSDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:vecArray forKey:@"vec"];
    [dict setValue:decArray forKey:@"decoded"];
    //    NSData *authData = [vecArray dataUsingEncoding:NSUTF8StringEncoding];
    //    NSString *authHeader = [NSString stringWithFormat: @"Basic %@",
    //                            [authData base64EncodedStringWithOptions:0]];
    
    
    //    NSArray *jsonObject = [NSJSONSerialization JSONObjectWithData:[vecArray dataUsingEncoding:NSUTF8StringEncoding]
    //                                                          options:0 error:NULL]
    NSData *requestData = [NSData dataWithBytes:[vecArray UTF8String] length:[vecArray lengthOfBytesUsingEncoding:NSUTF8StringEncoding]];
    requestData =   [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:icyURL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    NSLog(@"\nRequest : %@", request);
    NSLog(@"\nNSDATA : %@", [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);
    
    NSURLSessionConfiguration *sessionConfig =
    [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session =
    [NSURLSession sessionWithConfiguration:sessionConfig delegate: (id)self delegateQueue:nil];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *_Nullable data, NSURLResponse *_Nullable response, NSError *_Nullable error) {
                                      if (error) {
                                          // do something with the erroE
                                          return;
                                      }
                                      NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                      if (httpResponse.statusCode == 201) {
                                          NSArray* arrTokenData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                          NSLog(@"%@", arrTokenData);
                                          //[self postDomain];
                                      } else {
                                          // failure: do something else on failure
                                          NSLog(@"httpResponse code: %@", [NSString stringWithFormat:@"%ld", (unsigned long)httpResponse.statusCode]);
                                          NSLog(@"httpResponse head: %@", httpResponse.allHeaderFields);
                                          
                                          return;
                                      }
                                  }];
    [task resume];
}

//- (void)dealloc {
//    [TextView release];
//    [recordOutlet release];
//    [super dealloc];
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//##########################################################################################
#pragma mark
//------------------------------------------------------------------------------
#pragma mark - AUDIO FUNCTIONS
//---------------------------------
#pragma mark - feedSamples
//------------------------------------------------------------------------------

ShortBuffer strbuffer;            //OBJECT for Stringbuffer class
void *refToSelf;
int ct=0;
int check_result_flag=0;
//@ct -> Our own count for circular buffer for storing "shorts" in string, value goes upto @STORAGE_VECTOR
//@check_result_flag -> Gives back OTP result as well as other flags
//                   0 -> Continue to check trigger values for every @BUFFER_SIZE "shorts"
//                  -3 -> When trigger value is detected
//                  -4 -> When the next @TOTAL_CHUNKS_HAVING_DATA are being stored after detecting -3
//                  -5 -> When the final buffer is stored, so that ASYNCHRONOUS thread can be run when CORRELATION calculations will happen
//                  -2 -> When calculation for OTP is going on
//                  >0 -> OTP detected ; -1 -> Wrong OTP

- (void)feedSamplesToEngine:(UInt32)audioDataBytesCapacity audioData:(void *)audioData
{
    if(audioDataBytesCapacity > BUFFER_SIZE)
        return; //#Handles error where overflow is there.
    int sampleCount = audioDataBytesCapacity / sizeof(SAMPLE_TYPE);
    SAMPLE_TYPE *samples = (SAMPLE_TYPE*)audioData;
    std::string shorts;
    double power = pow(2,10);
    for(int i = 0; i < sampleCount; i++) {
        //#EXTREME IMPORTANCE: the data recieved is in a format where the endianess,i.e, format of binary is in a different way
        //This is done to correct that error for "16BIT" binary number
        SAMPLE_TYPE sample_swap =  (0xff00 & (samples[i] << 8)) | (0x00ff & (samples[i] >> 8)) ; //Endianess issue
        char dataInterim[30];
        sprintf(dataInterim,"%f ", sample_swap/power); // normalize it.
        shorts.append(dataInterim);
    }
    //Checking trigger
    double trigger_result =1;
    if(check_result_flag==0){
        Trigger tg; //initiated for later use
        trigger_result = tg.isFound(shorts );
        NSLog(@"\nTrigger for chunk no. %d : %f",++ct, trigger_result);
    }
    
    //Sending to paste to our own circular buffer
    ct = ct%(STORAGE_VECTOR);
    // To prevent retain cycles call back by weak reference
    //__weak __typeof(self) weakSelf = self;  // New C99 uses __typeof(..)
    // Heavy work dispatched to a separate thread
//    dispatch_barrier_async(concurrentQueue, ^{
//        NSLog(@"Hi, I'm the final block!\n");
//    });
//    dispatch_async( concurrentQueue , ^{
    // Do heavy or time consuming work
    //__typeof(weakSelf) strongSelf = weakSelf;
    //if (strongSelf) {
    
        //*********@check :
        //*********0:continue recording, -2:Final Calculation of OTP going on, -3:Point where trigger is detected,-4 means dont read trigger next time as value of check is global
    int send_to_corr;
    send_to_corr = strbuffer.putshortsinarray(shorts, ct, trigger_result);
    check_result_flag = send_to_corr;
    dispatch_async(dispatch_get_main_queue(), ^{
        if(check_result_flag == -2 ){
            [self stopRecording];
            NSLog(@"Stopping recording, entry in -2");
        }
         if(check_result_flag>0 || check_result_flag==-1){
            [self stopRecording]; //NOTE: Enabling this here and inside check==-2 , throws ERROR
            [self changeOTP:send_to_corr];
            [appDelegate ChangeFlagSwift:0];
             [self startRecording];
        }
        else if(check_result_flag==-3){
            [appDelegate ChangeFlagSwift:1];
        }
            });
       // }
//    });
}

-(void)changeOTP:(int)myOTP{
    //FADEOUT
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:0.1];
    //[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    self.TextView.alpha = 0;
    [UIView commitAnimations];

    //NOTE : Asynchronous task because UICHanges can only be done on the main thread
    if(myOTP==-1)
        self.TextView.text= [NSString stringWithFormat:@"-no OTP"];
    else
        self.TextView.text= [NSString stringWithFormat:@"%d", myOTP];
    //Run main thread to update UI
    //FADEin
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:0.8];
    //[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    self.TextView.alpha = 1;
    [UIView commitAnimations];
}



//------------------------------------------------------------------------------
#pragma mark - CallBack
//------------------------------------------------------------------------------
//.........................................................................................................
//##########################################################################################

// Takes a filled buffer and writes it to disk, "emptying" the buffer
void AudioInputCallback(void * inUserData,
                        AudioQueueRef inAQ,
                        AudioQueueBufferRef inBuffer,
                        const AudioTimeStamp * inStartTime,
                        UInt32 inNumberPacketDescriptions,
                        const AudioStreamPacketDescription * inPacketDescs)
{
    RecordState * recordState = (RecordState*)inUserData;
    if (!recordState->recording)
    {
        printf("Not recording, returning\n");
        return; //important otherwise takes global value of check in its other buffers.
    }
    //     if (inNumberPacketDescriptions == 0 && recordState->dataFormat.mBytesPerPacket != 0)
    //     {
    //         inNumberPacketDescriptions = inBuffer->mAudioDataByteSize / recordState->dataFormat.mBytesPerPacket;
    //     }
    printf("Writing buffer %lld\n", recordState->currentPacket);
    OSStatus status = AudioFileWritePackets(recordState->audioFile,
                                            false,
                                            inBuffer->mAudioDataByteSize,
                                            inPacketDescs,
                                            recordState->currentPacket,
                                            &inNumberPacketDescriptions,
                                            inBuffer->mAudioData);
    if (status == 0) {
        recordState->currentPacket += inNumberPacketDescriptions;
    }
    else cout<<"Status:"<<status;
    AudioQueueEnqueueBuffer(recordState->queue, inBuffer, 0, NULL);

    //######Searched for REMOTEIO API by tastypixel.com, TPCIrcularBuffer
    //    AudioBufferList bufferList;
    //    bufferList.mNumberBuffers = 20;
    //    for(int j=0; j<bufferList.mNumberBuffers; j++)
    //    {   bufferList.mBuffers[j].mNumberChannels = 1;
    //        bufferList.mBuffers[j].mData = NULL;
    //        bufferList.mBuffers[j].mDataByteSize = recordState->dataFormat.mFramesPerPacket * sizeof(SInt16) * 2;
    //
    //        // Put audio into circular buffer
    //        TPCircularBufferProduceBytes(&recordState->c_buffer, bufferList.mBuffers[j].mData, bufferList.mBuffers[j].mDataByteSize);
    //    }
    
    //Do stuff with samples recorded
    if(check_result_flag == -4 ){
        NSDate *date = [NSDate date];
        ViewController *rec = (__bridge ViewController *) refToSelf;
        [rec feedSamplesToEngine:inBuffer->mAudioDataBytesCapacity audioData:inBuffer->mAudioData];
        double timePassed_ms = [date timeIntervalSinceNow] * -1000.0;
        if(timePassed_ms>31)
            cout<<"\nTIME: "<<timePassed_ms;
        }
    else {
        dispatch_async(concurrentQueue, ^{
            cout<<"in";
            NSDate *date = [NSDate date];
            ViewController *rec = (__bridge ViewController *) refToSelf;
            [rec feedSamplesToEngine:inBuffer->mAudioDataBytesCapacity audioData:inBuffer->mAudioData];
            double timePassed_ms = [date timeIntervalSinceNow] * -1000.0;
            if(timePassed_ms>31)
                cout<<"\nTIME: "<<timePassed_ms;
       });
    }
}

//....................................... Fills an empty buffer with data and sends it to the speaker
void AudioOutputCallback(void * inUserData,
                         AudioQueueRef outAQ,
                         AudioQueueBufferRef outBuffer)
{
    PlayState* playState = (PlayState*)inUserData;
    if(!playState->playing)
    {
        printf("Not playing, returning\n");
        return;
    }
    
    printf("Queuing buffer %lld for playback\n", playState->currentPacket);
    
    
    AudioStreamPacketDescription* packetDescs = NULL;
    
    
    UInt32 bytesRead;
    UInt32 numPackets = BUFFER_SIZE/2;
    //playState->mNumPacketsToRead ; //BUFFER_SIZE/2; because see sampleCount is 2048 in feedSamplesToEngine function
    OSStatus status;
    status = AudioFileReadPackets(playState->audioFile,
                                  false,
                                  &bytesRead,
                                  packetDescs,
                                  playState->currentPacket,
                                  &numPackets,
                                  outBuffer->mAudioData);
    //cout<<numPackets;
    if (numPackets>0)
    {
        outBuffer->mAudioDataByteSize = bytesRead;
        //status = AudioQueueEnqueueBuffer(outAQ, outBuffer, 0, NULL);
        status = AudioQueueEnqueueBuffer(playState->queue,
                                                 outBuffer,
                                                 0,
                                                 packetDescs);
        
        playState->currentPacket += numPackets;
    }
    else
    {
        if (playState->playing)
        {
            AudioQueueStop(playState->queue, false);
            AudioFileClose(playState->audioFile);
            playState->playing = false;
        }
        
        AudioQueueFreeBuffer(playState->queue, outBuffer);
    }
    
}
//.##########################################################################################..

//------------------------------------------------------------------------------
#pragma mark - AudioFormat
//------------------------------------------------------------------------------

- (void)setupAudioFormat:(AudioStreamBasicDescription*)format
{
    //SAMPLE_TYPE is short datatype
    format->mSampleRate = SAMPLE_RATE;
    format->mFormatID = kAudioFormatLinearPCM;
    format->mFramesPerPacket = 1;
    format->mChannelsPerFrame = 1;
    format->mBytesPerFrame = sizeof(SAMPLE_TYPE) * NUM_CHANNELS; //2;
    format->mBytesPerPacket = sizeof(SAMPLE_TYPE) * NUM_CHANNELS;    //2;
    format->mBitsPerChannel = 8 * sizeof(SAMPLE_TYPE);          //16;
    format->mReserved = 0;
    format->mFormatFlags = kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked | kLinearPCMFormatFlagIsBigEndian ;
    
    
}

- (void)recordPressed:(id)sender
{
    if (!playState.playing)
    {
        if (!recordState.recording)
        {
            printf("Starting recording\n");
            [self startRecording];
        }
        else
        {
            printf("Stopping recording\n");
            [self stopRecording];
        }
    }
    else
    {
        printf("Can't start recording, currently playing\n");
    }
}

- (void)playPressed:(id)sender
{
    if (!recordState.recording)
    {
        if (!playState.playing)
        {
            printf("Starting playback\n");
            [self startPlayback];
        }
        else
        {
            printf("Stopping playback\n");
            [self stopPlayback];
        }
    }
}

//#############################

- (void)startRecording
{   self.TextView.text = @"Recording";
    [self setupAudioFormat:&recordState.dataFormat];
    NSLog(@"Sample : %f",recordState.dataFormat.mSampleRate);
    
    recordState.currentPacket = 0;
    
    
    OSStatus status;
    status = AudioQueueNewInput(&recordState.dataFormat,
                                AudioInputCallback,
                                &recordState,
                                CFRunLoopGetCurrent(),
                                kCFRunLoopCommonModes,
                                0,
                                &recordState.queue);
    if (status == 0)
    {
        // Prime recording buffers with empty data
        for (int i = 0; i < NUM_BUFFERS; i++)
        {
            AudioQueueAllocateBuffer(recordState.queue, BUFFER_SIZE , &recordState.buffers[i]);
            AudioQueueEnqueueBuffer (recordState.queue, recordState.buffers[i], 0, NULL);
        }
        
        status = AudioFileCreateWithURL(fileURL, //if returns -50, then fileURL= " "should have path
                                        kAudioFileAIFFType,
                                        &recordState.dataFormat,
                                        kAudioFileFlags_EraseFile,
                                        &recordState.audioFile);
        
        if (status == 0)
        {   NSLog(@"Entered");
            recordState.recording = true;
            status = AudioQueueStart(recordState.queue, NULL);
            if (status == 0)
            {
                labelStatus.text = @"Recording";
            }
        }
    }
    
    if (status != 0)
    {
        NSLog(@"Statys : %d",(int)status);
        [self stopRecording];
        labelStatus.text = @"Record Failed";
    }
    
    
}

- (void)stopRecording
{
    recordState.recording = false;
    
    AudioQueueStop(recordState.queue, true);
    for(int i = 0; i < NUM_BUFFERS; i++)
    {
        AudioQueueFreeBuffer(recordState.queue, recordState.buffers[i]);
    }
    
    AudioQueueDispose(recordState.queue, true);
    AudioFileClose(recordState.audioFile);
    labelStatus.text = @"Idle";
}

//#####################################

- (void)startPlayback
{
    playState.currentPacket = 0;
    [self setupAudioFormat:&playState.dataFormat];
    OSStatus status;
    status = AudioFileOpenURL(fileURL, kAudioFileReadPermission, kAudioFileAIFFType, &playState.audioFile);
    
    if (status == 0) {
        status = AudioQueueNewOutput(&playState.dataFormat,
                                     AudioOutputCallback,
                                     &playState,
                                     CFRunLoopGetCurrent(),
                                     kCFRunLoopCommonModes,
                                     0,
                                     &playState.queue);
        if (status == 0) {
            // Allocate and prime playback buffers
            playState.playing = true;
            for (int i = 0; i < NUM_BUFFERS && playState.playing; i++)
            {
                
                AudioQueueAllocateBuffer(playState.queue, BUFFER_SIZE, &playState.buffers[i]);
                AudioOutputCallback(&playState, playState.queue, playState.buffers[i]);
            }
            
            status = AudioQueueStart(playState.queue, NULL);
            if (status == 0)
            {
                labelStatus.text = @"Playing";
            }
        }
    }
    
    if (status != 0)
    {
        [self stopPlayback];
        labelStatus.text = @"Play failed";
    }
}

- (void)stopPlayback
{
    playState.playing = false;
    
    for(int i = 0; i < NUM_BUFFERS; i++)
    {
        AudioQueueFreeBuffer(playState.queue, playState.buffers[i]);
    }
    
    AudioQueueDispose(playState.queue, true);
    AudioFileClose(playState.audioFile);
}

//------------------------------------------------------------------------------
#pragma mark - END
//------------------------------------------------------------------------------

- (BOOL)getFilename:(char*)buffer maxLenth:(int)maxBufferLength
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString* docDir = [paths objectAtIndex:0];
    NSString* file = [docDir stringByAppendingString:@"/recording.aif]"];
    return [file getCString:buffer maxLength:maxBufferLength encoding:NSUTF8StringEncoding];
}

////#####################################################Pod Function
//// EZMicrophoneDelegate will provide these callbacks
//-(void)microphone:(EZMicrophone *)microphone
// hasAudioReceived:(float **)buffer
//   withBufferSize:(UInt32)bufferSize
//withNumberOfChannels:(UInt32)numberOfChannels {
//    dispatch_async(dispatch_get_main_queue(),^{
//        // Updates the audio plot with the waveform data
//        [self.audioPlot updateBuffer:buffer[0] withBufferSize:bufferSize];
//    });
//}
//
//-(void)microphone:(EZMicrophone *)microphone hasAudioStreamBasicDescription:(AudioStreamBasicDescription)audioStreamBasicDescription {
//    // The AudioStreamBasicDescription of the microphone stream. This is useful when configuring the EZRecorder or telling another component what audio format type to expect.
//
//    // We can initialize the recorder with this ASBD
//    self.recorder = [EZRecorder recorderWithDestinationURL:[self testFilePathURL]
//                                           andSourceFormat:audioStreamBasicDescription];
//
//}
//
//-(void)microphone:(EZMicrophone *)microphone
//    hasBufferList:(AudioBufferList *)bufferList
//   withBufferSize:(UInt32)bufferSize
//withNumberOfChannels:(UInt32)numberOfChannels {
//
//    // Getting audio data as a buffer list that can be directly fed into the EZRecorder. This is happening on the audio thread - any UI updating needs a GCD main queue block. This will keep appending data to the tail of the audio file.
//    if( TRUE ){
//        [self.recorder appendDataFromBufferList:bufferList
//                                 withBufferSize:bufferSize];
//    }
//
//}



@end

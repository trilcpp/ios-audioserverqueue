////
////  GPGPU.metal
////  AudioServerQueue
////
////  Created by Shivansh on 7/21/17.
////
////
//
////#ifdef __cplusplus
////#include <string>
////#include <iostream>
////using namespace std;
////#endif
//
//#include <stdio.h>
//#include <metal_stdlib>
//#include "Finddata.hpp"
//using namespace metal;
//
//
//id <MTLDevice> mtlDevice = MTLCreateSystemDefaultDevice();
//id <MTLCommandQueue> mtlCommandQueue = [mtlDevice newCommandQueue];
//id <MTLCommandBuffer> mtlCommandBuffer = [mtlCommandQueue commandBuffer];
//
//id <CAMetalDrawable> frameDrawable;
//frameDrawable = [renderLayer nextDrawable];
//MTLRenderPassDescriptor *mtlRenderPassDescriptor;
//mtlRenderPassDescriptor = [MTLRenderPassDescriptor new];
//mtlRenderPassDescriptor.colorAttachments[0].texture = frameDrawable.texture;
//mtlRenderPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
//mtlRenderPassDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(0.75, 0.25, 1.0, 1.0);
//
//id <MTLRenderCommandEncoder> renderCommand = [mtlCommandBuffer renderCommandEncoderWithDescriptor: mtlRenderPassDescriptor];
//// Set MTLRenderPipelineState
//// Draw objects here
//[renderCommand endEncoding];
//
//kernel int sigmoid(const device string divided) {
//    // This calculates sigmoid for _one_ position (=id) in a vector per call on the GPU
//    
//    FindData fdata;//Class name for findata funcitons
//    std::pair<int,int> pair_result[DIVIDE_TOTAL_CHUNKS_INTO];
//    
//    //For async
//    std::pair<int,int> *pairs = pair_result;
//    string *divided = divided_storage ;
//    FindData *fd; fd = &fdata;
//    
//    
//    F(i,0,DIVIDE_TOTAL_CHUNKS_INTO){
//        cout<<"\n\n###";
//        pairs[i] = fd->apply( divided[i] );
//    }
//    
//    return pairs[1].first;
//}

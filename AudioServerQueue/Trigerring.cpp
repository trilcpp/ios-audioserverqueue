//
//  Trigerring.cpp
//  AudioQueue
//
//  Created by Shivansh on 6/21/17.
//
//

#include "Trigerring.hpp"
#include <vector>
#include <fstream>
#include <iostream>
#define Fs 44100
#define G .55
#define H 1.1

//Function defined public inside the class

double Trigger::isFound(cvec input) {

    cvec out = fft(input);
    vec mag = abs(out);
    int data_size = mag.size();
    
    vec amp = mag.get(0, (data_size/2)-1);
    vec filteredArray = getFilterdAmplitude(amp, data_size);
    vec chunkedData = filteredArray;
    
    int freq14Kindex = getIndexForFreq(14000, chunkedData.size(), 22050);
    
    double mean = getAvarage(
                             chunkedData,
                             freq14Kindex,
                             chunkedData.size()
                             );
    
    
    vec diversionValuesSquare = getDiversionSquare(chunkedData, mean, freq14Kindex);
    std::pair<double, double> resultGamma = getGammaValue(diversionValuesSquare, mean);
    double gammaValue = resultGamma.first;
    //double sigmaSqrValue = resultGamma.second;
    std::pair<double, double> result = checkCondition(chunkedData, gammaValue, mean);
    
    //double sigma = pow(sigmaSqrValue, 0.5);
    return result.second/result.first;
}

int getIndexForFreq(int freq, int len, int fs) {
    int index = ( ( len / (float) fs ) * freq);
    return index;
}

vec getFilterdAmplitude(vec amp, int size) {
    int sizeAmp = amp.size();
    int freqIndex14k = getIndexForFreq(14000, size, Fs);
    vec zeroArray = amp.get(0, freqIndex14k-1);
    zeroArray.zeros();
    return concat(zeroArray, amp.get(freqIndex14k, sizeAmp-1));
}

double getAvarage(vec Vec, int start, int end) {
    double avg = 0.0 ;
    double size = end - start;
    for (int i = start; i<end; i++) {
        avg += Vec[i];
    }
    return avg / size;
}

vec createChunks(vec amp, int chunk_length) {
    
    vec chunkedArray;
    int counter = 0;
    for (int i=0; i<amp.size(); i = i + chunk_length){
        double chunkSelect = getAvarage(amp, i, (i+chunk_length));
        chunkedArray.ins(counter, chunkSelect);
        counter = counter + 1;
    }
    
    return chunkedArray;
}

vec getDiversionSquare(vec amp, double mean, int Index14K) {
    vec diversionSquare;
    int counter = 0;
    
    for (int i=Index14K; i<amp.size(); i++) {
        double dataSelected = amp[i]-mean;
        dataSelected = dataSelected * dataSelected;
        diversionSquare.ins(counter, dataSelected);
        counter = counter + 1;
    }
    
    return diversionSquare;
}

std::pair<double, double> getGammaValue(vec diversionValuesSquare, double mean) {
    
    double sigmaSq = getAvarage(
                                diversionValuesSquare,
                                0,
                                diversionValuesSquare.size()
                                );
    
    double sigma = pow(sigmaSq, 0.5);
    double gamma = ( (G * sigmaSq) + (H * sigma) ) / mean;
    
    return make_pair(gamma, sigmaSq);
    
}

std::pair<double, double> checkCondition(vec amp, double gamma, double mean){
    int sizeAmp = amp.size();
    
    int freq14KIndex = getIndexForFreq(14000, sizeAmp, 22050) + 1;
    int freq16KIndex = getIndexForFreq(16000, sizeAmp, 22050) + 1;
    int freq18KIndex = getIndexForFreq(18000, sizeAmp, 22050) + 1;
    
    double gamma14K = 0.1;
    int counter14 = 0;
    double gamma16K = 0.1;
    int counter16 = 0;
    
    for( int i =freq14KIndex; i < freq18KIndex; i++) {
        if(i < freq16KIndex ) {
            if (abs(mean - amp[i]) > gamma) {
                gamma14K += amp[i];
                counter14 += 1;
                
            }
        }
        else {
            if (abs(mean - amp[i]) > gamma) {
                gamma16K += amp[i];
                counter16 += 1;
            }
        }
    }
    
    if (gamma14K > 0.1) {
        gamma14K = gamma14K / counter14;
    }
    if (gamma16K > 0.1) {
        gamma16K = gamma16K / counter16;
    }
    
    //double ratio = gamma16K / gamma14K;
    
    return make_pair(gamma14K, gamma16K);
}


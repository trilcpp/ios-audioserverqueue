//
//  correlate.cpp
//  AudioQueue
//
//  Created by Shivansh on 6/22/17.
//
//

#include "correlate.hpp"
#include <itpp/itcomm.h>

vec Correlate::apply(vec wave1, vec wave2) {
    int size1 = wave1.size();
    int size2 = wave2.size();
    
    
    vec resCorr = xcorr( wave1, wave2, -1, "none" );
    
    
    //We need an ouput matching python, which as corelation upto N-M+1
    //What we get is a size which has [ end -(N-1)+1 to end , 0 to (N-1)] concatenated.
    int N = std::max(size1, size2);
    int min = std::min(size1, size2);
    //Compute the FFT size as the "next power of 2" of the input vector's length (max)
    int b = static_cast<int>(std::ceil( log(N)/log(2)) );
    int fftsize = ((b < 0) ? 0 : (1 << b));
    int end=fftsize-1;
    
    int size = resCorr.size();
    int noOfElemsToRemoveFromBack = min ; //Since in python N-M+1
    resCorr.del(size-noOfElemsToRemoveFromBack, size-1);
    int noOfElemsToRemoveFromFront = end - ( end - (N-1) );
    resCorr.del(0,noOfElemsToRemoveFromFront-1);
    return resCorr;
}

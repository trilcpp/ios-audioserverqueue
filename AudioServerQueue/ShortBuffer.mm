//
//  ShortBuffer.cpp
//  OLA-C
//
//  Created by Shivansh on 7/4/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//
#import "ViewController.h"

#include "ShortBuffer.hpp"
#include "Finddata.hpp"

//int printMajority(int a[], const int &size);

int ShortBuffer::putshortsinarray(string const& shorts, int &buffer_count, double result)
{
    if(flag==2) //If operations for calculating otp ARE still running
    {   //NSLog(@"String2");
        return -2;
    }
    
    c_buffer[buffer_count].clear();    //If our buffer is filled, clear it
    c_buffer[buffer_count] = shorts;   //write to our buffer
        
    //Enters everytime after trigger is detected
    if(flag==1)
    {
        return WritetoStorage(buffer_count);
        
    }
    else if(result>75)
    {   flag=1;
        cout<<"asdf:result :: "<<result<<endl;
            //Clear data
            allBufferString.clear();
            allpairDecoded.clear();
        //When trigger is detected, start writing to @storage[]
        //To reduce comparison computation, not sending to Writetostorage()
        
        //Records previous 2 buffers, after the trigger is detected
        for(int j=mBUFFERS_BEFORE_TRIGGER; j>=1;j--)
        {
            int temp_buffer_count = (buffer_count-j < 0)? (STORAGE_VECTOR + buffer_count - j) : (buffer_count - j);
            storage[storagect++] = c_buffer[temp_buffer_count] ;
        }
        storage[storagect++] = c_buffer[buffer_count] ;
        return -3;   //NOT 0, so that Trigger calculation doesn't continue
    }
    else
        return 0;

}


int ShortBuffer::WritetoStorage(int buffer_count)
{
    cout<<"\nStorage count: "<<storagect<<endl;
    if(storagect == TOTAL_CHUNKS_HAVING_DATA + 1){
        storagect = 0;  //RESET
        flag=2;         //Made 2 so that nothing enters till all operations are completed
        return divideDataby();
    }
    
    else if(storagect == TOTAL_CHUNKS_HAVING_DATA) {
        for(int j=0; j<TOTAL_CHUNKS_HAVING_DATA ;j++)
        {   //cout<<"\n################Storage chunk :"<<j<<" "<<storage[j];
            allBufferString.append(storage[j]);
        }
        storagect++;
        return -5; //This would make the last WriteTostorage where calculations occur as async task
    }
    else {
        storage[storagect++]=c_buffer[buffer_count];
    }
    return -4; //NOT 0, so that Trigger calculation doesn't continue
}


//Dividing total number of chunks in storage[], da
int ShortBuffer::divideDataby()
{
    int division_sample_number = CHUNK_AFTER_SAMPLE;
    int sample_counter =0;
    
    string divided_storage[DIVIDE_TOTAL_CHUNKS_INTO];   //Array to store our final chunks, divided into respective parts.
    int count = 0; //Count for @divided_storage[], changes after every DIVIDE_CHUNK_AFTER_SAMPLE
    
    int ct_flag = 0; //Flag raised when the division is encountered. eg. to be divided after 1000 samples.
    //cout<<allBufferString;
    F(i,0, TOTAL_CHUNKS_HAVING_DATA)
    {
        for(int j=0; storage[i][j]!='\0'; j++)
        {
            if(storage[i][j]==' ')  //Note: sample_counter remains the same till the space isn't encountered.
                sample_counter++;
        
            if(sample_counter >= 0)
                {
                    if( (sample_counter % (division_sample_number) == 0) && storage[i][j]==' ' )
                        { ct_flag++;}
                 
                    if( ct_flag==1)
                        {   printf("\nCount %d & SAMPLE_count %d",count,sample_counter);
                            count++;
                            ct_flag=0;
                        }
                    if(count<DIVIDE_TOTAL_CHUNKS_INTO)
                        divided_storage[count].push_back(storage[i][j]) ;
                }
        }
    }
    cout<<"\nSample Total number : "<<sample_counter;
    cout<<"\nCount:"<<count;
    
    FindData fdata;//Class name for findata funcitons
    vec pair_result[DIVIDE_TOTAL_CHUNKS_INTO]; //Gets correlated arrays
    //For asyncnhronous, defining pointers to them
    vec *pairs = pair_result;
    string *divided = divided_storage ;
    FindData *fd; fd = &fdata;
    
    dispatch_queue_t myQ2 = dispatch_queue_create("MyQ2" , DISPATCH_QUEUE_CONCURRENT);
    dispatch_queue_t myGlobeQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_sync(myGlobeQueue , ^{
    dispatch_apply(DIVIDE_TOTAL_CHUNKS_INTO, myQ2, ^(size_t i) {
        cout<<"\n\n###";
        pairs[i] = fd->apply( divided[i] );
        cout<<"\\Test dispatch function shortbuffer";
        });
    });
    vec maxIndices1 = fdata.getPeaks_array(pair_result[0], 0);
    vec maxIndices2 = fdata.getPeaks_array(pair_result[1], CHUNK_AFTER_SAMPLE);
    int result_array[3];
    fdata.detect_distance(maxIndices1, maxIndices2, result_array);
    
    
    cout << "AFTER MAXINDICES FUNCTION IS RUN IN SHORTBUFFER";
    F(i,0,3) {
        char data[10];
        sprintf(data, "%d ", result_array[i]);
        allpairDecoded.append(data);
        cout<<result_array[i]<<" ";
    }
    
    linked_list list;
    const int sz = sizeof(result_array)/sizeof(int);
    int majority = list.showMajority_element(result_array, sz);
    cout << majority;
    
    sendToView();
    //##flag To show that all operations for calculating OTP are done. And function can be entered again.
    flag=0;
    return majority;
}


void ShortBuffer::sendToView()
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    ViewController *vc = [storyboard instantiateInitialViewController];
    
    
    NSString *BufferString = [[NSString alloc] initWithCString:allBufferString.c_str()
                                                      encoding:[NSString defaultCStringEncoding]];
    NSString *Pair = [[NSString alloc] initWithCString:allpairDecoded.c_str()
                                              encoding:[NSString defaultCStringEncoding]];
    
    //cout<<allBufferString;
    printf("sendToView() allBuffer Length: %lu",allBufferString.length());
    [vc storeToShorts:BufferString :Pair];
    allBufferString.clear();
    allpairDecoded.clear(); 
    return;
}




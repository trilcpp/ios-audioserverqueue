//
//  AudioRecorderAppDelegate.h
//  AudioRecorder
//
//  Copyright Shivansh Jagga. All rights reserved.
//

#import <UIKit/UIKit.h>



//.... The main application
@interface AudioRecorderAppDelegate : NSObject  <UIApplicationDelegate>
{
   //UIViewController *viewcontroller;
    
    UILabel* labelStatus;
    UIButton* buttonRecord;
    UIButton* buttonPlay;
    //TPCircularBuffer c_buffer;      //For buffers of circular queue we define this global object
}

@property (nonatomic, retain) UIWindow *window;
//trill, for accessing ui lavel
//@property (nonatomic, retain) UIViewController *viewcontroller;

-(void)ChangeFlagSwift:(NSInteger) flag;

@end

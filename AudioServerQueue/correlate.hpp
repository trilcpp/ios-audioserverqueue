//
//  correlate.hpp
//  AudioQueue
//
//  Created by Shivansh on 6/22/17.
//
//

#ifndef correlate_hpp
#define correlate_hpp

#include <itpp/itbase.h>

using namespace itpp;

class Correlate {
    std::pair<int, int> findDataPtFromDistBwPeaks(vec corrOut);
public:
    vec apply(vec data, vec sync);
    
};
#endif /* correlate_hpp */

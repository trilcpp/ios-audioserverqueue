//
//  ViewController.swift
//  Example-iOS
//
//  Created by Jonathan on 9/4/16.
//  Copyright © 2016 Jonathan Underwood. All rights reserved.
//

import UIKit
import AVFoundation
import WaveformView

var flag : NSInteger = 0
var ct : Double = 0.0

@objc class WaveViewController: UIViewController {
    var audioRecorder: AVAudioRecorder!
    
    
    @IBOutlet weak var waveformView: WaveformView!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        audioRecorder = audioRecorder(URL(fileURLWithPath:"/dev/null"))
        audioRecorder.record()

        let displayLink = CADisplayLink(target: self, selector: #selector(updateMeters))
        displayLink.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
    }

    func updateMeters() {
        audioRecorder.updateMeters()
        //NSLog("\n updateMeters()")
        let normalizedValue : Double
        if flag == 1 {
            if ct < 1.2 {
                ct = ct + 0.08
            }
        let raise = CGFloat(-1.4 + ct) //- CGFloat(audioRecorder.averagePower(forChannel: 0)/30)
        normalizedValue = Double( pow(10, raise) )
        }
        else {
            if ct > 0 {
                ct = ct - 0.015
            }
        normalizedValue = pow(10, -1.4 + ct)
        }
        
        waveformView.updateWithLevel(CGFloat(normalizedValue))
    }

    func audioRecorder(_ filePath: URL) -> AVAudioRecorder {
        let recorderSettings: [String : AnyObject] = [
            AVSampleRateKey: 44100.0 as AnyObject,
            AVFormatIDKey: NSNumber(value: kAudioFormatMPEG4AAC),
            AVNumberOfChannelsKey: 1 as AnyObject,
            AVEncoderAudioQualityKey: AVAudioQuality.min.rawValue as AnyObject
        ]

        try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)

        let audioRecorder = try! AVAudioRecorder(url: filePath, settings: recorderSettings)
        audioRecorder.isMeteringEnabled = true
        audioRecorder.prepareToRecord()

        return audioRecorder
    }
    //Comes from appDelegate
    func ChangeSwiftFlag( tflag: NSInteger) {        
        flag = tflag;

    }
    
    func hell() {
    }
}

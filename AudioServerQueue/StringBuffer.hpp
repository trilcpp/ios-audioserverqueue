//
//  StringBuffer.hpp
//  AudioQueue
//
//  Created by Shivansh on 6/23/17.
//
//
#ifdef __cplusplus
#include <string>
#include <iostream>
using namespace std;
#endif

#ifndef StringBuffer_hpp
#define StringBuffer_hpp

#include <stdio.h>


class StringBuffer {
    std::string storage[TOTAL_CHUNKS_HAVING_DATA];            //@storage is the final array of chunks of our whole data
    int flag;
    int storagect;
    string c_buffer[STORAGE_VECTOR];
    
public:
   
    
    StringBuffer()  //Constructor
    {flag=0; storagect=0;}
    
    int putshortsinarray(string const& shorts, int &buffer_count, double result);
    int WritetoStorage(int buffer_count);
    int divideDataby();
    
    //@ putshortsinarray() - Puts data into temporary array buffer,c_buffer
    //@ WritetoStorage() - After trigger is detected, the subsequent TOTAL_CHUNKS_HAVING_DATA are recorded into storage array
    //@ divideDataby() - Divides total data to be taken in a few parts, and retirieves data
};


#endif /* StringBuffer_hpp */

//
//  Finddata.hpp
//  AudioQueue
//
//  Created by Shivansh on 6/22/17.
//
//

#ifndef Finddata_hpp
#define Finddata_hpp

#import "itpp_headers.h"

using namespace itpp;
using namespace std;

class FindData {
    //The arguments include 'size' of the correlated array, and 'result_array' is where the results are stored
    int * findDataPtFromDistBwPeaks(vec corrOut,const int &size, int *result_array);
    //@biggest_peak -> returns height of biggest peak
    //@mark_every_frame -> returns vector array of indexes where the peaks start
    double biggest_peak(vec corr_op, const int &size);
    vector<int> mark_every_frame(vec corr_op, const int &size);
    
public:
    vec apply(vec inputWave);
    vec getPeaks_array(vec corrOut, const int &sepration);
    void detect_distance(vec maxIndices1, vec maxIndices2, int* result_array);
    
};


#pragma mark - HASH MAP
//HASH Map in the form of doubly linked list

//PRINT MAJORITY ELEMENT
struct majority_hash{
    int number;
    int count;
    struct majority_hash *next,*prev;
};

class linked_list{
private:
    majority_hash *head,*tail;
    int list_size, majority_result, majority_number_ct;
public:
    linked_list(){
        head = NULL; tail =NULL;
        list_size = 0;
        majority_result = -1;
        majority_number_ct = -1;
    }
    void increment_size()
    {   list_size += 1;
    }
    int get_size()
    {   return list_size;
    }
    int get_majority()
    {   if(majority_number_ct>=2)
        return majority_result;
        else
            return -1;
    }
    
    void add_node(int num){
        if(check_node_exists(num))  //Increments count if it exists
            return;
        increment_size();   //Increment-size if node doesn't exist
        majority_hash *temp = new majority_hash;
        temp->number = num;
        temp->count =1;
        temp->next = NULL;
        if(head==NULL){
            temp->prev = NULL;
            head = temp;
            tail = temp;
        }
        else {
            temp->prev = tail;
            tail->next = temp;
            tail = tail->next;
        }
        
        temp=NULL;
        free(temp);
        
    }//add_node
    
    //Checks if node exists and if it does, it increments count and returns true.
    bool check_node_exists(int &num){
        if(head==NULL)
            return false;
        if(head == tail)
        {   if(head->number == num) {
            majority_result = head->number;
            head->count = head->count + 1;
            majority_number_ct = head->count;
            return true;
        }
        else return false;
        }
        majority_hash *start = head;
        majority_hash *end = tail;
        int count = 0;
        while(count<list_size/2){
            if(start->number == num){
                start->count = start->count+1;
                if(start->count > majority_number_ct ){
                    majority_result = start->number;
                    majority_number_ct = start->count;
                    return true; //We want it to return before it even enters else if statement
                }
                else if(start->count == majority_number_ct ){   //Handles when no majority exists and two numbers have equal count
                    majority_result = -1;
                    majority_number_ct = start->count;
                }
                return true;
            }
            else if(end->number == num){
                end->count = end->count+1;
                if(end->count > majority_number_ct ){
                    majority_result = end->number;
                    majority_number_ct = end->count;
                    return true;
                }
                else if(end->count == majority_number_ct ){ //Handles when no majority exists and two numbers have equal count
                    majority_result = -1;
                    majority_number_ct = end->count;
                }
                return true;
            }
            start = start->next;
            end = end->prev;
            count++;
        } //While loop
        //        if(start->next == end->prev && start->next!=NULL){ //NULL condition handles it incase if 2 nodes are there.
        //            start = start->next;
        //            if(start->number == num){
        //                start->count = start->count+1;
        //                return TRUE;
        //            }
        //        }//if loop
        
        return false;
    }
    
    int majority_element(){
        if (head == NULL) return -100;
        if(list_size == 1) return head->number; //When only one number is there
        majority_hash *start = head;
        majority_hash *end = tail;
        int count = 0;
        int major_number = start->number;
        int major_number_count = start->count;
        start = start->next;
        while(count<list_size/2){ //No NULL for start->next because it wouldnt enter case of two nodes
            if(start->count >= end->count && start->count > major_number_count){
                major_number_count = start->count;
                major_number = start->number;
            }
            else if(start->count < end->count && end->count > major_number_count){
                major_number_count = end->count;
                major_number = end->number;
            }
            start = start->next;
            end = end->prev;
            count++;
        }//While loop
        if(major_number_count==0)
            return -1;
        
        return major_number;
    }
    
    int showMajority_element(int a[], const int &size)
    {
        F(i,0,size){
            if(a[i]!=-1)
               add_node(a[i]);
        }
        int majority = get_majority();
        return majority;
    }

};




#endif /* Finddata_hpp */

//
//  AudioRecorderAppDelegate.m
//  AudioRecorder
//
//  Copyright Shivansh Jagga. All rights reserved.
//
#import "ViewController.h"
#import "AudioRecorderAppDelegate.h"
#import "AudioQueue-Swift.h"

//##Interface is like defining a class

//Global interface variable defined in .h
//@interface AudioRecorderAppDelegate : NSObject {
//    
//}
//@end

@implementation AudioRecorderAppDelegate
@synthesize window;


//Initialization function
- init {
	if (self = [super init]) {
        
        //your initialization here
        NSLog(@"initialization");
        
        
       	}
	return self;
}

//Connector to Swift
-(void)ChangeFlagSwift:(NSInteger) flag{
    //WaveViewController *wc = (WaveViewController *)[[UIApplication sharedApplication] delegate];
    
    
    WaveViewController* wc = [[WaveViewController alloc]init];
    [wc ChangeSwiftFlagWithTflag:flag];

}


//- (void)dealloc
//{
//    [labelStatus release];
//    [buttonRecord release];
//    [buttonPlay release];
//    [window release];
//    [super dealloc];
//
//}

//.......................................................................................




- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    /*UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UIViewController *vc = [storyboard instantiateInitialViewController];
    
    // Set root view controller and make windows visible
    self.window.rootViewController = vc;
    [self.window makeKeyAndVisible];*/
    
    //ACCESS MAIN.STORYBOARD ELEMENTS
//    ViewController * viewController = (ViewController *)self.window.rootViewController;
//    NSLog(@"This is my text: %@", viewController.TextView.text);
    
   
    

    /*
    ///....................................THIS WAS previously inputted in the audioservices file
    // Create window
   self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    
    // Create status label
    labelStatus = [[UILabel alloc] initWithFrame: CGRectMake(10, 50, 300, 30)];
    labelStatus.textAlignment = UITextAlignmentCenter;
    labelStatus.numberOfLines = 1;
    labelStatus.text = @"Idle";
    
    // Create Record button
    buttonRecord = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    [buttonRecord setTitle:@"Record" forState:UIControlStateNormal];
    [buttonRecord addTarget:self action:@selector(recordPressed:)
           forControlEvents:UIControlEventTouchUpInside];
    //buttonRecord.center = CGPointMake(window.center.x, 200);
    buttonRecord.frame = CGRectMake(6.0, 120.0, 140, 35);
    buttonRecord.backgroundColor = [UIColor clearColor];
    
    // Create Play button
    buttonPlay = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    [buttonPlay setTitle:@"Play" forState:UIControlStateNormal];
    [buttonPlay addTarget:self action:@selector(playPressed:)
         forControlEvents:UIControlEventTouchUpInside];
    //buttonPlay.center = CGPointMake(window.center.x, 150);
    buttonPlay.frame = CGRectMake(161.0, 120.0, 140, 35);
    buttonPlay.backgroundColor = [UIColor clearColor];
    
    // Get audio file page
    char path[256];
    [self getFilename:path maxLenth:sizeof path];
    fileURL = CFURLCreateFromFileSystemRepresentation(NULL, (UInt8*)path, strlen(path), false);
    
    // Init state variables
    playState.playing = false;
    recordState.recording = false;
    
    // Add the controls to the window
    [window addSubview:labelStatus];
    [window addSubview:buttonRecord];
    [window addSubview:buttonPlay];
   
    [window makeKeyAndVisible];
   */
    
//////...............................
    
    
    return YES;
}



@end

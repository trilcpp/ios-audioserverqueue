//
//  NewBuffer.cpp
//  AudioServerQueue
//
//  Created by Shivansh on 7/8/17.
//
//

#include "NewBuffer.hpp"

//
//  ShortBuffer.cpp
//  OLA-C
//
//  Created by Shivansh on 7/4/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//
#import "ViewController.h"

#include "NewBuffer.hpp"
#include "Finddata.hpp"



int NewBuffer::putshortsinarray(string const& shorts, int &buffer_count, double result)
{
    c_buffer[buffer_count].clear();    //If our buffer is filled, clear it
    c_buffer[buffer_count] = shorts;   //write to our buffer
    
    //Enters everytime after trigger is detected
    if(flag==1)
    {
        return WritetoStorage(buffer_count);
        
    }
    
    else if(result>75)
    {   flag=1;
            //Clear data
            allBufferString.clear();
            allpairDecoded.clear();
        cout<<"Trigger detect:result :: "<<result<<endl;
        
        //When trigger is detected, start writing to @storage[]
        //To reduce comparison computation, not sending to Writetostorage()
        
        //Records previous 2 buffers, after the trigger is detected
        for(int j=mBUFFERS_BEFORE_TRIGGER; j>=1;j--)
        {
            int temp_buffer_count = (buffer_count-j < 0)? (STORAGE_VECTOR + buffer_count - j) : (buffer_count - j);
            storage[storagect++] = c_buffer[temp_buffer_count] ;
        }
        storage[storagect++] = c_buffer[buffer_count] ;
        
        return 0;   //So that recording continues
    }
    else
        return 0;
    
    
    
}


int NewBuffer::WritetoStorage(int buffer_count) //buffer_ct is the noraml STORAGE VECTOR COUNT - 4 in our case.
{
    cout<<"\nStorage count: "<<storagect<<endl;
    if(storagect == TOTAL_CHUNKS_HAVING_DATA)
    {
        flag=0;
        storagect = 0;
        
        for(int j=0; j<TOTAL_CHUNKS_HAVING_DATA ;j++)
        {   //cout<<"\n################Storage chunk :"<<j<<" "<<storage[j];
            allBufferString.append(storage[j]);
        }
        
        
        return divideDataby();
        
    }
    else
    {
        storage[storagect++]=c_buffer[buffer_count];
        return 0;
    }
    
    
}



//Dividing total number of chunks in storage[], da
int NewBuffer::divideDataby()
{
    int division_sample_number = CHUNK_AFTER_SAMPLE;
    int sample_counter =0;
    
    string divided_storage[DIVIDE_TOTAL_CHUNKS_INTO];   //Array to store our final chunks, divided into respective parts.
    int count = 0; //Count for @divided_storage[], changes after every DIVIDE_CHUNK_AFTER_SAMPLE
    
    int ct_flag = 0; //Flag raised when the division is encountered to increment @count. eg. to be divided after 1000 samples.
    
    //Divides the data
    //while (count!=DIVIDE_TOTAL_CHUNKS_INTO)
       F(i,0, TOTAL_CHUNKS_HAVING_DATA)
    {
        for(int j=0; storage[i][j]!='\0'; j++)
        {
            
            if(storage[i][j]==' ')  //Note: sample_counter remains the same till the space isn't encountered.
                sample_counter++;
        
            if(sample_counter!=0 && (sample_counter % (division_sample_number) == 0) && storage[i][j]==' ')
            { ct_flag++; }
            
            if( ct_flag==1)
            {   printf("\nCount %d & SAMPLE_count %d",count,sample_counter);
                count++;
                ct_flag=0;
            }

            if(count < DIVIDE_TOTAL_CHUNKS_INTO)
                divided_storage[count].push_back(storage[i][j]) ;
            
        }
    }
    cout<<"\nSample Total number : "<<sample_counter;
    cout<<"\nCount:"<<count;
    cout<<"\n\n";
    
    FindData fdata;// Class name for findata funcitons
    std::pair<int,int> pair_result[DIVIDE_TOTAL_CHUNKS_INTO];

    
        F(i,0 ,DIVIDE_TOTAL_CHUNKS_INTO)
    {
        cout<<"\n\n###"<<i;
        pair_result[i] = fdata.apply( divided_storage[i]);
        cout<<"\n\nFINAL RESULT::"<<pair_result[i].first<<"\n";
            //FORMAT
            char dataInterim[30];
            sprintf(dataInterim,"%d", pair_result[i].first); // normalize it.
            allpairDecoded.append(dataInterim);
            if(i!=DIVIDE_TOTAL_CHUNKS_INTO -1)
                allpairDecoded.append(",");
        //        if( ( i!=0 && (pair_result[i].first==pair_result[i-1].first) )|| ( (i-2)>=0 && (pair_result[i].first==pair_result[i-2].first)) )
        //        {   //Send : pair_result[i].first;
        //            cout<<"\n\nMajority Result::"<<pair_result[i].first<<endl;
        //            return pair_result[i].first;
        //            break;
        //        }
    }
    sendToView();
    return pair_result[1].first;
}


void NewBuffer::sendToView()
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    ViewController *vc = [storyboard instantiateInitialViewController];
    
    NSString *BufferString = [[NSString alloc] initWithCString:allBufferString.c_str()
                                                      encoding:[NSString defaultCStringEncoding]];
    
    NSString *Pair = [[NSString alloc] initWithCString:allpairDecoded.c_str()
                                                      encoding:[NSString defaultCStringEncoding]];
    
    //cout<<allBufferString;
    cout<<endl<<"sendToView() allBuffer Length: "<<allBufferString.length();
    [vc storeToShorts:BufferString :Pair];
    return;
}
